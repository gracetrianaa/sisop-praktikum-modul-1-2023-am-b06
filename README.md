# sisop-praktikum-modul-1-2023-AM-B06

Anggota kelompok B06 adalah :

|        Nama           | NRP|
| ---                   |--- |
|Glenaya                |5025211202 |
|Gracetriana Survinta  Septinaputri | 5025211199 |
|Ken Anargya Alkausar   | 5025211168 |


# Laporan Resmi Praktikum Modul 1 Sisop

## Soal Nomor 1
>> Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 
>>- Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
>>- Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
>>- Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
>>- Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

### Pembahasan 1A
Jawaban
```shell script
echo "Nomor 1a"
grep -m 5 Japan '2023 QS World University Rankings.csv'
```
Penjelasan

Soal meminta menampilkan 5 Universitas dengan ranking tertinggi di Jepang yang terdapat di file .csv. Sehingga menggunakan command ```grep -m 5 Japan``` untuk mencari 5 data pertama yang mengandung kata Jepang di dalam file 2023 QS World University Rankings.csv. 

### Pembahasan 1B
Jawaban
```shell script
echo -e "\nNomor 1b"
grep Japan '2023 QS World University Rankings.csv' | sort -t, -k 9n | head -n 5
```
Penjelasan

Soal meminta untuk menampilkan Universitas dengan Faculty Student Score (fsr score) terendah di Jepang berdasarkan file 2023 QS World University Rankings.csv. Sehingga menggunakan command ```grep Japan '2023 QS World University Rankings.csv'``` untuk mencari data yang mengandung kata Jepang di dalam file .csv tersebut. Lalu, menggunakan command ```sort -t, -k 9n``` untuk mengurutkan data tersebut berdasarkan kolom ke 9 dengan urutan dari yang kecil ke yang besar. Setelah itu menggunakan command ```head -n 5``` untuk mengambil lima data yang terdapat di paling atas. 

### Pembahasan 1C
Jawaban
```shell script
echo -e "\nNomor 1c"
grep Japan '2023 QS World University Rankings.csv' |sort -t, -k 20n | grep -m 10 Japan
```
Penjelasan

Soal meminta untuk menampilkan 10 universitas di Jepang yang memiliki Employment Outcome Rank (ger rank) tertinggi. Pertama menggunakan ```grep Japan '2023 QS World University Rankings.csv'``` untuk mencari data yang mengandung kata Jepang dalam file .csv tersebut. Setelah itu, menggunakan ```sort -t, -k 20n``` untuk mengurutkan data tersebut berdasarkan kolom 20 secara numerik. Terakhir menggunakan ```grep -m 10 Japan``` untuk mengambil 10 data paling atas dari hasil sortingan sebelumnya. 

### Pembahasan 1D
Jawaban
```shell script
echo -e "\nNomor 1d"
echo "apa kata kunci universitas paling keren? "
read univ
paling_keren="keren"

if [ $paling_keren == $univ ]
then grep Keren '2023 QS World University Rankings.csv'

else 
echo "SALAH, SILAHKAN COBA LAGI"
fi
```
Penjelasan

Soal meminta untuk menampilkan universitas paling keren di dunia dengan menggunakan kata kunci "keren". 
- Pertama yang dilakukan adalah mendeklarasi ```paling_keren``` sebagai kata kuncinya lalu ```univ``` sebagai kata yang akan dimasukan bocchi sebagai kata kunci universitas paling keren. 
- Kedua menggunakan kondisi ```if else``` untuk membandingkan kata kunci dengan kata yang dimasukan oleh bocchi
- Jika kata kunci yang dimasukkan sama dengan variabel string ```paling_keren```yaitu "keren" (kondisi _true_) maka program akan menampilkan kata kunci Keren yang terdapat di dalam file '2023 QS World UniversitY Rankings.csv'.
- Jika kondisi _false_  dimana kata kunci yang dimasukan berbeda dengan yang sudah dideklarasi yaitu "keren" maka akan menghasilkan _output_ ```SALAH, SILAHKAN COBA LAGI```






## Soal Nomor 2
>> Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
>>- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
>>- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

### Pembahasan 2A
Jawaban
```shell script
hour=$(date +"%k")
num_downloads=$hour

if [ $hour -eq 0 ]; then
    num_downloads=1
fi

awal='kumpulan_'
n=1

while [ -d /home/user/"$awal$n" ]; do ((n++)); done

mkdir /home/user/"$awal$n"

for (( i=1; i<=$num_downloads; i++ ))
do
    filename="perjalanan_$i.jpg"
    wget "https://source.unsplash.com/1920x1080/?indonesia" -O "$filename"
    mv "$filename" /home/user/kumpulan_$n
done

# command untuk melakukan crontab 10 jam sekali
#* */10 * * * /home/ken/Documents/Praktikum1/kobeni_perjalanan.sh
```

Penjelasan

```shell script
hour=$(date +"%k")
num_downloads=$hour
```
mendapatkan x download sesuai dengan jam saat ini dalam format 24 jam

```shell script
if [ $hour -eq 0 ]; then
    num_downloads=1
fi
```
apabila waktu menunjukkan pukul 00.00 maka cukup download 1 gambar saja

```shell script
awal='kumpulan_'
n=1

while [ -d /home/user/"$awal$n" ]; do ((n++)); done

mkdir /home/user/"$awal$n"
```
membuat direktori baru dengan nama kumpulan_ di dalam direktori 'home/user'. Nama direktori diikuti dengan nomor urutan yang dimulai dari angka 1 dan bertambah sesuai dengan urutan direktori yang sudah ada.

```shell script
for (( i=1; i<=$num_downloads; i++ ))
do
    filename="perjalanan_$i.jpg"
    wget "https://source.unsplash.com/1920x1080/?indonesia" -O "$filename"
    mv "$filename" /home/user/kumpulan_$n
done
```
melakukan pengunduhan gambar secara random dari website source.unsplash.com yang dijalankan sebanyak ```$num_downloads```. dan akan dipindahkan ke direktori ```/home/user/kumpulan_$n```

``` shell script
* */10 * * * /home/ken/Documents/Praktikum1/kobeni_perjalanan.sh
```
penggunaan cron job untuk menjalankan kobeni.perjalanan.sh setiap 10 jam sekali, dan digunakan crontab -e pada terminal jika ingin mengedit crontab task yang telah dibuat

### Pembahasan 2B
Jawaban
```shell script
zip_name='devil_' 
j=1

while [ -d ~/"$zip_name$j" ]; do 
    ((j++))
done

mkdir ~/"$zip_name$j"

prefset='kumpulan_'
k=1

while [ -d ~/"$prefset$k" ]; do 
    ((k++))
done

for ((i=1; i<k; i++))
do
    mv ~/$prefset$i ~/$zip_name$j
done

zip -r ~/$zip_name$j.zip ~/$zip_name$j && echo "Zip file has been created successfully." || echo "failed to create zip file"

rm -r ~/$zip_name$j
```

Penjelasan

```shell script
zip_name='devil_' 
j=1

while [ -d ~/"$zip_name$j" ]; do 
    ((j++))
done

mkdir ~/"$zip_name$j"
```
membuat direktori dengan nama "devil_" diikuti nomor yang bertambah jika direktori dengan nama yang sama sudah ada. perulangan digunakan untuk mengecek apakah direktori dengan nama "devil_nomor" sudah ada. jika sudah ada maka variabel a akan bertambah. dan digunakan mkdir untuk membuat direktorinya.

```shell script
prefset='kumpulan_'
k=1

while [ -d ~/"$prefset$k" ]; do 
    ((k++))
done

for ((i=1; i<k; i++))
do
    mv ~/$prefset$i ~/$zip_name$j
done
```
mencari direktori terbaru dengan nama yang dimulai dengan prefix "kumpulan_" yang ditentukan oleh variabel prefset, kemudian memindah semua file ke direktori "devil_nomor".

```shell script
zip -r ~/$zip_name$j.zip ~/$zip_name$j && echo "Zip file has been created successfully." || echo "failed to create zip file"

rm -r ~/$zip_name$j
```
membuat file zip dengan nama "devil_nomor urut" dan mengompres file yang ada di direktori tersebut, kemudian jika berhasil akan teroutput kalimat "Zip file has been created successfully." dan jika gagal akan teroutput "failed to create zip file", kemudian direktori "devil_nomor urut" akan dihapus setelah file zip dibuat.

#### Kendala
Saat praktikum, terdapat kendala pada soal 2B, yaitu saat memasukkan file "kumpulan_" ke dalam zip "devil_", terdapat kesalahan pada perulangan for pada script yang mengakibatkan program tidak berjalan sesuai yang diminta soal.








## Soal Nomor 3

Peter Griffin hendak membuat suatu sistem register pada script `louis.sh` dari setiap user yang berhasil didaftarkan di dalam file `/users/users.txt`. Peter Griffin juga membuat sistem login yang dibuat di script `retep.sh`

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
>>- Minimal 8 karakter
>>- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
>>- Alphanumeric
>>- Tidak boleh sama dengan username 
>>- Tidak boleh menggunakan kata chicken atau ernie

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
>>- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
>>- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
>>- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
>>- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in


### Jawaban

Pertama-tama, membuat file `log.txt` dan directory ```users``` yang menyimpan ```users.txt```
```shell script
touch log.txt
mkdir users
touch users.txt
```
Kemudian, membuat shell `louis.sh` dengan perintah nano
```shell script
nano louis.sh
```

#### louis.sh

```shell script
#!/bin/bash

dd=$(date +%D)
time=$(date +%T)
```

- Deklarasi variabel `dd` dan `time` untuk format YY/MM/DD hh:mm:ss di `log.txt`

```shell script
echo "Enter username: "
read username
```
- Menggunakan perintah `read username` untuk user input username 

```shell script
if awk -F: '{print $1}' ./users/users.txt | grep -q "^$username"; then
  echo "Username already exist. Please try again."
  echo "$dd $time REGISTER: ERROR User already exists" >> log.txt
  exit 1
fi
```
- Dalam kondisi if, `awk -F: '{print $1}'` akan melakukan pemisahan field dengan delimiter ":" dan mengambil baris pertama dari field `./users/users.txt`
- Dilakukan juga `grep -q username` dimana akan mencari string username yang ada di `./users/users.txt`
- Jika kondisi if terpenuhi (ditemukan username yang sama), maka akan mengeluarkan output username already exist dan menambahkan MESSAGE pada `log.txt`


```shell script
while true; do
  echo "Enter password: "
  read -s password

  if [[ "$password" == "$username" ]]; then
    echo "Password must be different from username"
    continue
  fi
  if [[ "$password" == "ernie" || "$password" == "chicken" ]]; then
    echo "Password can not be ernie or chicken"
    continue
  fi
  if [ ${#password} -lt 8 ]; then
    echo "Password must at least 8 characters long"
    continue
  fi
  if [[ !("$password" =~ [a-z]) || !("$password" =~ [A-Z]) || !("$password" =~ [0-9]) ]]; then
    echo "Password must have at least 1 uppercase character, 1 lowercase character, and 1 numerical character"
    continue
  fi
  if [[ !("$password" =~ ^[[:alnum:]]+$) ]]; then
    echo "Password can not contain non alphanumeric character"
    continue
  fi
  break
done
```
- Menggunakan while loop, memeriksa jika kondisi benar/true maka akan melakukan validasi password
- Melakukan `read password` untuk input password
- Baris-baris selanjutnya berisi kode untuk validasi password, yaitu:
>>- Jika password sama dengan username, maka akan mengeluarkan `"Password must be different from username"` dengan kondisi `if [[ "$password" == "$username" ]]`
>>- Jika password yang diinput adalah ernie atau chicken, maka akan mengeluarkan `"Password can not be ernie or chicken"` dengan kondisi `if [[ "$password" == "ernie" || "$password" == "chicken" ]]`
>>- Jika password kurang dari 8 karakter, maka akan mengeluarkan `"Password must at least 8 characters long"` dengan kondisi `if [ ${#password} -lt 8 ]`
>>- Jika password yang diinput tidak mengandung uppercase, lowercase, dan angka, maka akan mengeluarkan `"Password must have at least 1 uppercase character, 1 lowercase character, and 1 numerical character"` dengan kondisi `if [[ !("$password" =~ [a-z]) || !("$password" =~ [A-Z]) || !("$password" =~ [0-9]) ]]`
>>- Jika password yang diinput tidak mengandung alphanumeric karakter, maka akan mengeluarkan `"Password can not contain non alphanumeric character"` dengan kondisi `if [[ !("$password" =~ ^[[:alnum:]]+$) ]]`
- Jika semua syarat password sudah terpenuhi maka akan `break` dari loop

```shell script
echo "$username $password" >> ./users/users.txt

echo "Registration successful"

echo "$dd $time REGISTER: INFO User $username registered successfully" >> log.txt
```
- Setelah melakukan pengecekan atau validasi password, `username` dan `password` yang didaftarkan akan disimpan ke dalam `./users/users.txt` dan mengeluarkan `Registration successful`
- MESSAGE `"$dd $time REGISTER: INFO User $username registered successfully"` akan disimpan ke dalam `log.txt`


#### retep.sh

Untuk membuat file ini, digunakan perintah nano
```shell script
nano retep.sh
```
Script ini digunakan untuk login. Berhasil atau tidaknya tergantung dengan username dan password yang telah didaftarkan sebelumnya di `louis.sh`. 

```shell script
#!/bin/bash

dd=$(date +%D)
time=$(date +%T)

echo "Enter username: "
read username
echo "Enter password: "
read -s password
echo
```
- Sama seperti di `louis.sh`, deklarasi variabel `dd` dan `time` untuk format MESSAGE pada `log.txt`
- Input username dan password menggunakan command `read username` dan `read -s password`, `-s` akan melakukan silent input (tidak mengeluarkan apapun saat diketik)

```shell script
if grep -q "^$username $password" ./users/users.txt; then
  echo "Login successful!"
  echo "$dd $time LOGIN: INFO User $username logged in" >> log.txt
```
- Menggunakan `grep -q` untuk mencari username dan password dari dalam `./users/users.txt`
- Jika ditemukan username dan passwordnya, maka akan menyatakan `"Login successfull!"` dan menyimpan MESSAGE User berhasil login ke `log.txt`

```shell script
else
  echo "Login failed!"
  echo "$dd $time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi
```
- Jika username dan password tidak terdapat pada `./users/users.txt` maka akan mengeluarkan `"Login failed!"` dan MESSAGE bahwa login tidak berhasil yang dimasukkan ke dalam `log.txt` kemudian akan keluar dari program







## Soal Nomor 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
>>- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
>>- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
>>- Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀

### Jawaban
Buat file `log_encrypt.sh` terlebih dahulu dengan
```shell script
nano log_encrypt.sh
```
#### log_encrypt.sh
```shell script
#!/bin/bash

time=$(date +%H)
```
- Deklarasi variabel time yang akan mengambil `hour` back up dilakukan dan nantinya akan menjadi kunci pergeseran huruf

```shell script
fileformat=$(date +"%H:%M %d:%m:%Y")
```
- Deklarasi variabel fileformat yang berisi format "jam:menit tanggal:bulan:tahun" untuk penamaan file txt dari back up

```shell script
lowercase=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

start="${lowercase:0:26}${uppercase:0:26}"
shifted="${lowercase:${time}:26}${uppercase:${time}:26}"
```
- Variabel `lowercase` menyatakan abjad a-z dalam bentuk lowercase, dan variabel `uppercase` menyatakan abjad A-Z dalam bentuk uppercase
- Variabel `start` berisi string dengan urutan abjad secara normal (urutan 0-26) untuk lowercase dan uppercase
- Variabel `shifted` berisi string dengan urutan abjad yang dimulai dari `{time}` ke `{time} + 25` untuk lowercase dan uppercase

```shell script
encrypt() {
tr "${start}" "${shifted}" <<< $(cat /var/log/syslog)  > "/home/grace/syslog_backup/${fileformat}.txt"
}

encrypt
```
- Fungsi `encrypt()` akan berisi script untuk menjalankan sistem cipher untuk mengenkripsi file
- Di dalam fungsi `encrypt()`, command `cat` akan melihat isi file `/var/log/syslog`
- Command `tr` digunakan untuk mentranslate atau mengubah string dari bentuk `{start}` ke `{shifted}` atau dari bentuk urutan abjad normal menjadi urutan abjad yang telah digeser sesuai kunci (time). Misalnya, jika back up dilakukan pada pukul 12.00, urutan 0:26 menjadi 12:26 'abcdefghijklmnopqrstuvwxyz' akan menjadi 'mnopqrstuvwxyzabcdefghijkl` sehingga huruf b akan menjadi huruf n setelah bergeser
- Hasil dari pergeseran abjad atau enkripsi akan disimpan ke dalam `"/home/grace/syslog_backup/${fileformat}.txt"`
- Pemanggilan fungsi `encrypt()` dilakukan dengan menuliskan nama fungsi yaitu `encrypt`



Membuat script `log_decrypt.sh` dengan 
```shell script
nano log_decrypt.sh
```

#### log_decrypt.sh
```shell script
#!/bin/bash

time=$(date +%H)
```
- Sama seperti `log_encrypt.sh`, deklarasi variabel `time` yang akan mengambil waktu file syslog dibackup

```shell script
backupfile=$(ls -t /home/grace/syslog_backup | head -n1)
decrypted_file="/home/grace/syslog_backup_decrypted/${backupfile}"
```
- `ls -t` akan mengurutkan isi file dari `/home/grace/syslog_backup` dimulai dari yang terbaru dan `head -n1` akan mengambil baris atau list teratas untuk didekripsi, command-command tersebut diinisialisasi ke dalam `backupfile`
- `decrypted_file` berisi path lokasi untuk menyimpan file yang telah didekripsi nantinya

```shell script
lowercase=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

start="${lowercase:0:26}${uppercase:0:26}"
shifted="${lowercase:${time}:26}${uppercase:${time}:26}"
```
- Mendeklarasikan abjad a-z ke dalam `lowercase` dan abjad A-Z ke dalam `uppercase`
- Sama seperti `log_encrypt.sh` juga, variabel `start` akan menyimpan string abjad dengan urutan normal (0-26) untuk lowercase dan uppercase sedangkan variabel `shifted` menyimpan hasil pergeseran abjad sesuai kunci (time)

```shell script
decrypt() {
  tr "${shifted}" "${start}" <<< $(cat "/home/grace/syslog_backup/${backupfile}") > "${decrypted_file}"
}

decrypt
```
- Membuat fungsi `decrypt()` untuk menjalankan command yang akan mendekripsi file
- Di dalam fungsi tersebut, diambil dari file `/home/grace/syslog_backup/${backupfile}` untuk dienkripsi menggunakan command `cat`
- `tr` akan mentranslate/mengubah string dari string yang telah terenkripsi sebelumnya menjadi string dengan urutan normal, maka dari itu urutannya `{shifted}` ke `{start}`, terbalik dengan `log_encrypt.sh` tadi
- Hasil file yang telah terdekripsi akan disimpan ke dalam directory/folder yang telah dideklarasikan tadi sebagai `${decrypted_file}`
- Pemanggilan fungsi dilakukan dengan menuliskan `decrypt`

#### Cron Jobs

Membuat crontab file terlebih dahulu dengan perintah:
```shell script
crontab -e
```
Karena diminta untuk melakukan back up tiap 2 jam sekali maka di dalam crontab akan dijalankan:

```shell script
0 */2 * * * /home/grace/log_encrypt.sh
0 */2 * * * /home/grace/log_decrypt.sh
```
#### Kendala
Saat praktikum kemarin, `log_decrypt.sh` masih error sehingga output yang dihasilkan masih berupa file yang terenkripsi. Setelah ditelusuri dan dilakukan revisi, ternyata terdapat kesalahan penulisan script yang mengakibatkan script tidak berjalan dengan benar.


