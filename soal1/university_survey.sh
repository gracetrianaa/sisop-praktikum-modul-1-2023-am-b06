#!/bin/bash

echo "Nomor 1a"
grep -m 5 Japan '2023 QS World University Rankings.csv'

echo -e "\nNomor 1b"
grep  Japan '2023 QS World University Rankings.csv' | sort -t, -k 9n | head -n 5

echo -e "\nNomor 1c"
grep Japan '2023 QS World University Rankings.csv' |sort -t, -k 20n | grep -m 10 Japan

echo -e "\nNomor 1d"
echo "apa kata kunci universitas paling keren? "
read univ
paling_keren="keren"

if [ $paling_keren == $univ ]
then grep Keren '2023 QS World University Rankings.csv'

else 
echo "SALAH, COBA LAGI"
fi
