#!/bin/bash

time=$(date +%H)

fileformat=$(date +"%H:%M %d:%m:%Y")

lowercase=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

start="${lowercase:0:26}${uppercase:0:26}"
shifted="${lowercase:${time}:26}${uppercase:${time}:26}"

encrypt() {
tr "${start}" "${shifted}" <<< $(cat /var/log/syslog)  > "/home/grace/syslog_backup/${fileformat}.txt"
}

encrypt

# 0 */2 * * * /home/grace/log_encrypt.sh

