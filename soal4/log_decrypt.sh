#!/bin/bash

time=$(date +%H)


backupfile=$(ls -t /home/grace/syslog_backup | head -n1)
decrypted_file="/home/grace/syslog_backup_decrypted/${backupfile}"

lowercase=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')
uppercase=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')

start="${lowercase:0:26}${uppercase:0:26}"
shifted="${lowercase:${time}:26}${uppercase:${time}:26}"

decrypt() {
  tr "${shifted}" "${start}" <<< $(cat "/home/grace/syslog_backup/${backupfile}") > "${decrypted_file}"
}

decrypt

# 0 */2 * * * /home/grace/log_decrypt.sh




