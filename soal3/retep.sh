#!/bin/bash

dd=$(date +%D)
time=$(date +%T)

echo "Enter username: "
read username
echo "Enter password: "
read -s password
echo

if grep -q "^$username $password" ./users/users.txt; then
  echo "Login successful!"
  echo "$dd $time LOGIN: INFO User $username logged in" >> log.txt
else
  echo "Login failed!"
  echo "$dd $time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi
