#!/bin/bash

dd=$(date +%D)
time=$(date +%T)


echo "Enter username: "
read username

if awk -F: '{print $1}' ./users/users.txt | grep -q "^$username"; then
  echo "Username already exist. Please try again."
  echo "$dd $time REGISTER: ERROR User already exists" >> log.txt
  exit 1
fi

while true; do
  echo "Enter password: "
  read -s password

  if [[ "$password" == "$username" ]]; then
    echo "Password must be different from username"
    continue
  fi
  if [[ "$password" == "ernie" || "$password" == "chicken" ]]; then
    echo "Password can not be ernie or chicken"
    continue
  fi
  if [ ${#password} -lt 8 ]; then
    echo "Password must at least 8 characters long"
    continue
  fi
  if [[ !("$password" =~ [a-z]) || !("$password" =~ [A-Z]) || !("$password" =~ [0-9]) ]]; then
    echo "Password must have at least 1 uppercase character, 1 lowercase character, and 1 numerical character"
    continue
  fi
  if [[ !("$password" =~ ^[[:alnum:]]+$) ]]; then
    echo "Password can not contain non alphanumeric character"
    continue
  fi
  break
done


echo "$username $password" >> ./users/users.txt

echo "Registration successful"

echo "$dd $time REGISTER: INFO User $username registered successfully" >> log.txt
