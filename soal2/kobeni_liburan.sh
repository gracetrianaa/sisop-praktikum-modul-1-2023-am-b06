!/bin/bash

hour=$(date +"%k")
num_downloads=$hour

if [ $hour -eq 0 ]; then
    num_downloads=1
fi

awal='kumpulan_'
n=1

while [ -d ~/"$awal$n" ]; do ((n++)); done

mkdir ~/"$awal$n"

for (( i=1; i<=$num_downloads; i++ ))
do
    filename="perjalanan_$i.jpg"
    wget "https://source.unsplash.com/1920x1080/?indonesia" -O "$filename"
    mv "$filename" ~/kumpulan_$n
done

# command untuk melakukan crontab setiap 10 jam sekali

# * */10 * * * /home/user/kobeni_perjalanan.sh

# 2B

zip_name='devil_' 
j=1

while [ -d ~/"$zip_name$j" ]; do 
    ((j++))
done

mkdir ~/"$zip_name$j"

prefset='kumpulan_'
k=1

while [ -d ~/"$prefset$k" ]; do 
    ((k++))
done

for ((i=1; i<k; i++))
do
    mv ~/$prefset$i ~/$zip_name$j
done

zip -r ~/$zip_name$j.zip ~/$zip_name$j && echo "Zip file has been created successfully." || echo "failed to create zip file"

rm -r ~/$zip_name$j

# command untuk melakukan crontab setiap 1 hari sekali

# * * */1 * * /home/user/kobeni_perjalanan.sh



